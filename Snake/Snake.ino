//-----------------------
// Name : Arduino - Snake
// Date : 21.03.2016
// Version : 0.5v
//-----------------------
// To Do
// - check newApple behind tail of snake
// - check fight width vs length
//-----------------------
/*INCLUDE*/
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
//#include <Adafruit_NeoPixel.h> using for what?
#include <Button.h>

/*DEFINE*/
// Data of game
#define WALLS           false
#define LENGTH          32
#define WIDTH           18
#define STARTSIZE       3
#define SPEEDSTART      300
#define SPRITETIMELIMIT 100
#define BRIGHTNESS      50

// number of PIN
#define PINDISPLAY  6
#define TOUCHUP     0  // Connect to GND for work
#define TOUCHDOWN   4  // Connect to GND for work
#define TOUCHLEFT   1  // Connect to GND for work
#define TOUCHRIGHT  2  // Connect to GND for work

// Color definitions
#define BLACK   matrix.Color(0, 0, 0)
#define BLUE    matrix.Color(0, 0, 255)
#define CYAN    matrix.Color(0, 255, 255)
#define GREEN   matrix.Color(0, 255, 0)
#define MAGENTA matrix.Color(255, 0, 255)
#define RED     matrix.Color(255, 0, 0)
#define WHITE   matrix.Color(255, 255, 255)
#define YELLOW  matrix.Color(255, 255, 0)

/*STRUCT*/
typedef struct Coordinated {
  unsigned char x;
  unsigned char y;
} Coordinated;

/*VARIABLE*/
bool endGame = false;
bool leftDirection = false;
bool rightDirection = false;
bool upDirection = true;
bool downDirection = false;
int sizeSnake = STARTSIZE;
Coordinated apple;
Coordinated snake[50];
// Class button of library button
Button bLeft  = Button(TOUCHLEFT, PULLUP);
Button bRight = Button(TOUCHRIGHT,PULLUP);
Button bUp    = Button(TOUCHUP,   PULLUP);
Button bDown  = Button(TOUCHDOWN, PULLUP);

// Configuration small table
/* matrixWidth, matrixHeight, tilesX, tilesY, pin,
Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(32, 8, 1, 2, PINDISPLAY,
                            NEO_MATRIX_TOP      + NEO_MATRIX_LEFT +
                            NEO_MATRIX_COLUMNS  + NEO_MATRIX_ZIGZAG +
                            NEO_TILE_TOP        + NEO_TILE_LEFT +
                            NEO_TILE_COLUMNS    + NEO_TILE_ZIGZAG,
                            // matrixType, ledType
                            NEO_RGB             + NEO_KHZ800);*/
// Configuration big table
// matrixWidth, matrixHeight, tilesX, tilesY, pin,
Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(32, 18, PINDISPLAY,
                            NEO_MATRIX_TOP      + NEO_MATRIX_LEFT +
                            NEO_MATRIX_ROWS     + NEO_MATRIX_ZIGZAG, 
                            NEO_RGB             + NEO_KHZ800);

// Method setup
void setup() {
  matrix.begin();
  matrix.setTextWrap(false);
  matrix.setBrightness(BRIGHTNESS);
  matrix.setTextColor(matrix.Color(255, 0, 0));
  newSnake();
  newApple();
}

// loop of software
void loop() {
  if (!endGame) {
    drawTable();
    verifyDirection();

    if (WALLS) { // If use wall
      movement();
      checkCollisionWithWall();
    } else {
      movementWithoutWalls();
      checkSnakeBites();
    }

    checkApple();
    timeBetweenSprite();
  } else {
    gameOver();

    if (bUp.isPressed()) { // new Party
      sizeSnake = STARTSIZE;
      newSnake();
      newApple();
      endGame = false;
    }
  }
}

// Checks if the snake eats apple
void checkApple() {
  if ((snake[0].x == apple.x) && (snake[0].y == apple.y)) {
    sizeSnake++;
    newApple();
  }
}

// Control the limits of the screen and if snake eats
void checkCollisionWithWall() {
  checkSnakeBites();

  // Check collision with wall
  if (snake[0].x < 0)
    endGame = true;
  if (snake[0].x >= LENGTH)
    endGame = true;
  if (snake[0].y < 0)
    endGame = true;
  if (snake[0].y >= WIDTH)
    endGame = true;
}

// Check collision with the tail of the snake
void checkSnakeBites() {
  for (int counterSize = sizeSnake; counterSize > 0; counterSize--) {
    if ((counterSize > STARTSIZE) && (snake[0].x == snake[counterSize].x) && (snake[0].y == snake[counterSize].y)) {
      endGame = true;
    }
  }
}

// Drawing the snake
void drawSnake() {
  for (int counterSize = 0; counterSize < sizeSnake; counterSize++) {
    matrix.drawPixel(31 - snake[counterSize].x, snake[counterSize].y, GREEN);
  }
}

// Drawing all the game board with the snake and the apple
void drawTable() {
  matrix.fillScreen(0);
  drawSnake();
  matrix.drawPixel(LENGTH-1 - apple.x, apple.y , RED); // drawApple
  matrix.show();
}

// Poster "game over" with red background
void gameOver() {
  int x = matrix.width();
  matrix.setBrightness(BRIGHTNESS);
  matrix.fillScreen(0);
  while (x > -80)
  {      
    matrix.fillScreen(RED);
    matrix.setCursor(x, 4);
    matrix.print(F("GAME OVER"));
    matrix.setTextColor(WHITE);
    x--;

    matrix.show();
    delay(50);
  }
}

// Performs the movements of the snake
void movement() {
  shiftingBodySnake();

  // Moves the head of snake
  if (leftDirection) {
    snake[0].x--;
  }
  if (rightDirection) {
    snake[0].x++;
  }
  if (upDirection) {
    snake[0].y++;
  }
  if (downDirection) {
    snake[0].y--;
  }
}

// Performs the movements of the snake
void movementWithoutWalls() {
  shiftingBodySnake();

  // Moves the head of snake and look at the infinite sides
  if (leftDirection) {
    if (snake[0].x <= 0) {
      snake[0].x = LENGTH  - 1;
    } else
      snake[0].x--;
  }
  if (rightDirection) {
    if (snake[0].x >= LENGTH - 1) {
      snake[0].x = 0;
    } else
      snake[0].x++;
  }
  if (downDirection) {
    if (snake[0].y <= 0)
    {
      snake[0].y = WIDTH - 1;
    } else
      snake[0].y--;
  }
  if (upDirection) {
    if (snake[0].y >= WIDTH - 1)
    {
      snake[0].y = 0;
    } else
      snake[0].y++;
  }
}

// Change the coordinate of the apple
void newApple() {
  apple.x = random(LENGTH);
  apple.y = random(WIDTH);
}

// Between the first coordinate of the snake
void newSnake() {
  for (int cournterSize = 0; cournterSize < sizeSnake; cournterSize++) {
    snake[cournterSize].x =  5;
    snake[cournterSize].y = 5 + cournterSize;
  }
}

// Moves the table for the body of the snake
void shiftingBodySnake() {
  for (int cournterSize = sizeSnake; cournterSize > 0; cournterSize--) {
    snake[cournterSize].x = snake[(cournterSize - 1)].x;
    snake[cournterSize].y = snake[(cournterSize - 1)].y;
  }
}

// Calcule the time betwwen two sprite
void timeBetweenSprite() {
  int timeSprite = SPEEDSTART;
  if (timeSprite < SPRITETIMELIMIT) {
    timeSprite = SPRITETIMELIMIT;
  } else {
    timeSprite = SPEEDSTART - (sizeSnake * 10);
  }
  delay(timeSprite);
}

// Check the input and change direction
void verifyDirection() {
  if ((bRight.isPressed()) && (!leftDirection)) {
    rightDirection = true;
    upDirection = false;
    downDirection = false;
  }
  if ((bLeft.isPressed()) && (!rightDirection)) {
    leftDirection = true;
    upDirection = false;
    downDirection = false;
  }
  if ((bUp.isPressed()) && (!downDirection)) {
    upDirection = true;
    rightDirection = false;
    leftDirection = false;
  }
  if ((bDown.isPressed()) && (!upDirection)) {
    downDirection = true;
    rightDirection = false;
    leftDirection = false;
  }
}





