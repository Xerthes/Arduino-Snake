#include <Button.h>

Button button = Button(9,PULLUP);

// Method setup
void setup() {
  Serial.begin(9600);
  Serial.println("Start");
}

// loop of software
void loop() {
  if(button.uniquePress()) {
    Serial.println("Hello World");
  }
}
